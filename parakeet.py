# Copyright (C) 2018  Abitha K Thyagarajan
#
# parakeet is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# parakeet is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# parakeet.  If not, see <http://www.gnu.org/licenses/>.

import gnupg # for encrypting and decrypting email
from taskw import TaskWarrior # taskwarrior api
from premailer import transform # for inlining css
import ruamel.yaml as yaml # for config file
from imapclient import IMAPClient as imapc # for getting email
import smtplib # for sending email
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart # for working with email messages
import validators # for linkify()

# config file
with open("config.yml", 'r') as ymlfile:
    config = yaml.safe_load(ymlfile)

# variables
tw = TaskWarrior()
gpg = gnupg.GPG()
#etc.

# log in
# for reading email
reader = imapc(config['gmail']['imap_server'], use_uid = True, ssl = True)
rv = reader.login(config['task-email'], config['password'])
# for sending email
sender = smtplib.SMTP_SSL(config['gmail']['smtp_server'], config['gmail']['smtp_port'])
#sender.starttls()
sender.login(config['task-email'], config['password'])

# go to inbox/all mail
if config['x-gm-raw'] or config['is-gmail']:
    mailboxes = reader.list_folders()
    mailboxes = [str(x) for x in mailboxes]
    allmail = str("")
    for a in mailboxes:
        if "\All" in a:
            allmail = a[a.index('['):a.index(')', -2)]
    allmail = allmail[:-1]
    inbox = reader.select_folder(allmail)
else:
    inbox = reader.select_folder('INBOX')


# search for emails with "task" in subject
#messages = reader.search(['UNSEEN SUBJECT ' + config['flags']['task']])
if config['x-gm-raw']:
    messages = reader.gmail_search("in:unread " + config['flags']['task'])
else:
    messages = reader.search(['SUBJECT ' + config['flags']['task']])
pprint(messages)

# get email bodies

# decrypt (recipient is repo-email)
# https://pythonhosted.org/gnupg/gnupg.html#gnupg-module

# add, delete tasks, whatever

# reply (if applicable)

# prettifying
def linkify(s):
    """if s is an email:
        return <a href='mailto:s'>s</a>
    elif s is a URL:
        return <a href='s'>s</a>
    elif s is a phone number:
        return <a href='tel:s'>s</a>
    else:
        return s
    """

#task list html
def generate_report(s = 'pending', fields = ['id', 'description']):
    #s = pending, completed
    report = ["", ""]
    report[1] = "<table>"
    for i in tw.load_tasks()[s]:
        report[1] += "<tr>"
        for j in fields:
            report[0] += str(i[j]) + "\t"
            report[1] += "<td>" + str(i[j]) + "</td>"
        report[0] += "\n"
        report[1] += "</tr>"
    report[1] += "</table>"
    return report

# add generate_report() to the body of the email
email_html = generate_report()[1]

# inline the css (if stylesheet is provided)
def inline_css():
    if config['css'] != "no":
        email_html = transform("<head><link rel='stylesheet' href='" + config['css'] + "'></head>" + email)

# encrypt reply (recipient is main-email)
encryptedemail = MIMEMultipart('alternative')
encryptedemail['Subject'] = "Sent from parakeet"
encryptedemail['From'] = config['task-email']
encryptedemail['To'] = config['main-email']

# inline pgp
email_text = MIMEText(text, gpg.encrypt(email_text, config['main-email'], sign = config['task-key'].fingerprint, passphrase = config['passphrase']))
email_html = MIMEText(html, gpg.encrypt(email_html, config['main-email'], sign = config['task-key'].fingerprint, passphrase = config['passphrase']))

encryptedemail.attach(email_text)
encryptedemail.attach(email_html)

# send it!
sender.send_message(encryptedemail)

# mark as read / delete emails

# log out
reader.logout()
sender.quit()

print(generate_report()[1])
